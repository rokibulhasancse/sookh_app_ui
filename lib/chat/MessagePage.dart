import 'package:flutter/rendering.dart';
import 'package:fluttertestapp/chat/ChatPage.dart';
import 'package:fluttertestapp/custom/common/custom_order_ui.dart';
import 'package:fluttertestapp/order/OrderDetailsPage.dart';
import 'package:fluttertestapp/notification/AppNotificationPage.dart';
import 'package:fluttertestapp/order/OrderPage.dart';
import 'package:fluttertestapp/profile/ProfilePage.dart';
import 'package:fluttertestapp/custom/common/custom_column_inside_container_and_text.dart';
import 'package:fluttertestapp/custom/common/custom_order_status_ui.dart';
import 'package:fluttertestapp/model/NotificationModel.dart';
import 'package:flutter/material.dart';

class MessagePage extends StatefulWidget {
  @override
  ListViewHome createState() {
    return new ListViewHome();
  }
}

class ListViewHome extends State<MessagePage> {
  List<NotificationModel> notiModel = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    notiModel.add(new NotificationModel(1, "Computer", "", "",
        "A computer is a machine that can store and process information. Most computers rely on a binary system that uses two variables"));
    notiModel.add(new NotificationModel(1, "Laptop", "", "",
        "Laptops combine all the input/output components and capabilities of a desktop computer"));
    notiModel.add(new NotificationModel(1, "Android", "", "",
        "Android is a mobile operating system based on a modified version of the Linux kernel."));
    notiModel.add(new NotificationModel(1, "IPhone", "", "",
        "Details of the product based on a modified version of the Linux kernel."));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.grey.shade50,
          automaticallyImplyLeading: false,
          toolbarHeight: 120,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                child: Row(
                  children: [
                    InkWell(
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ),
                    Text(
                      ' Message',
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    )
                  ],
                ),
              ),
              Row(
                children: [
                  Expanded(
                      child: InkWell(
                    child: CustomColumnInsideText(
                        notiCount: 5,
                        title: "Chats",
                        icon: Icon(Icons.chat),
                        color: Colors.green),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChatPage()));
                    },
                  )),
                  Expanded(
                      child: InkWell(
                    child: CustomColumnInsideText(
                        notiCount: 5,
                        title: "Notifications",
                        icon: Icon(Icons.notifications),
                        color: Colors.blue),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AppNotificationPage()));
                    },
                  )),
                  Expanded(
                      child: InkWell(
                    child: CustomColumnInsideText(
                        notiCount: 5,
                        title: "Orders",
                        icon: Icon(Icons.reorder),
                        color: Colors.orange),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => OrderPage()));
                    },
                  )),
                  Expanded(
                      child: InkWell(
                    child: CustomColumnInsideText(
                        notiCount: 5,
                        title: "Promos",
                        icon: Icon(Icons.production_quantity_limits),
                        color: Colors.redAccent),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfilePage()));
                    },
                  )),
                ],
              ),
            ],
          )),
      body: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.all(10),
                child: Text(
                  "Last 7 Days",
                  style: TextStyle(fontSize: 13),
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: notiModel.length,
                    itemBuilder: (context, index) {
                      return Card(
                          //     elevation: 0,
                          // color: Colors.yellow.shade50,
                          child: Container(
                        margin: EdgeInsets.all(12),
                        child: Column(
                          children: [
                            IntrinsicHeight(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(30),
                                        color: Colors.redAccent),
                                    child:
                                        Icon(Icons.production_quantity_limits),
                                  ),
                                  Expanded(
                                    flex: 8,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(8, 8, 8, 8),
                                          child: Text(
                                            notiModel.elementAt(index).notiHead,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 12,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                            padding:
                                                EdgeInsets.fromLTRB(8, 0, 8, 8),
                                            child: Text(
                                              // notiModel.elementAt(index).notiHead,
                                              "10.34 AM",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.grey,
                                                fontSize: 10,
                                              ),
                                            )),
                                      ],
                                      // )
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              //shadowColor: Colors.grey,
                              margin: const EdgeInsets.all(12),
                              child: IntrinsicHeight(
                                child: Column(
                                  children: <Widget>[
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: Image.asset(
                                        'assets/instragram.jpeg',
                                        width: double.infinity,
                                        height: 120.0,
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                                      child: Text(
                                        notiModel.elementAt(index).notiBody,
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 12),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ));
                    }),
              ),
            ],
          )),
    );
  }
}
