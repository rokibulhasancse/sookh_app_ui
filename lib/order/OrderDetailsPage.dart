import 'package:flutter/material.dart';
import 'package:fluttertestapp/custom/common/CustomAppBar.dart';
import 'package:fluttertestapp/custom/common/custom_order_status_ui.dart';

class OrderDetails extends StatefulWidget {
  const OrderDetails({Key? key}) : super(key: key);

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false, // Used for removing back buttoon.
        backgroundColor: Colors.grey.shade50,
        title: CustomAppBar(
          title: " Order Details",
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Ship & bill to",
                style: TextStyle(fontSize: 14, color: Colors.grey),
              ),
              Text(
                "Name",
                style: TextStyle(fontSize: 14, color: Colors.grey),
              ),
              Text(
                "Phone",
                style: TextStyle(fontSize: 14, color: Colors.grey),
              ),
              Text(
                "Address",
                style: TextStyle(fontSize: 13, color: Colors.grey),
              ),
              Divider(
                thickness: 5,
                color: Colors.grey,
              ),
              //CustomOrderUi(),
              Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Package 1",
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Sold by Electric world Corporation > ",
                          style: TextStyle(
                              fontSize: 10,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold),
                        ),
                        Text("Unpaid",
                            style: TextStyle(fontSize: 11, color: Colors.grey))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                            flex: 3,
                            child: Image.network(
                                "https://static.javatpoint.com/tutorial/flutter/images/flutter-logo.png")),
                        Expanded(
                          flex: 7,
                          child: Container(
                            margin: EdgeInsets.all(8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Laptops combine all the input/output components and capabilities of a desktop computer, including the display screen, small speakers, a keyboard, data storage device, sometimes an optical disc drive, pointing devices (such as a touchpad or pointing stick), with an operating system, a processor and memory into a single.",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.black,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                Text("5000",
                                    style: TextStyle(
                                        fontSize: 11, color: Colors.grey)),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "x 1",
                                      style: TextStyle(
                                          fontSize: 11, color: Colors.grey),
                                    ),
                                    Text("Cancelled",
                                        style: TextStyle(
                                            fontSize: 11,
                                            color: Colors.red,
                                            fontStyle: FontStyle.italic))
                                  ],
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 5,
                color: Colors.grey,
              ),

              Text(
                "Order 454357457435485938475",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.green),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Placed on 04 Jun 2021 16:04:04 ",
                    style: TextStyle(
                        fontSize: 10,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold),
                  ),
                  Text("Unpaid",
                      style: TextStyle(fontSize: 11, color: Colors.grey))
                ],
              ),
              Divider(
                thickness: 5,
                color: Colors.grey,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Subtotal",
                    style: TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                  Text(
                    "22,480",
                    style: TextStyle(fontSize: 13, color: Colors.black),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Shipping Fee",
                    style: TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                  Text(
                    "70",
                    style: TextStyle(fontSize: 13, color: Colors.black),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Total Saving",
                    style: TextStyle(fontSize: 13, color: Colors.grey),
                  ),
                  Text(
                    "22,480",
                    style: TextStyle(fontSize: 13, color: Colors.black),
                  ),
                ],
              ),
              Container(
                color: Colors.grey,
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Shipping Fee Promotion",
                      style: TextStyle(fontSize: 13, color: Colors.black),
                    ),
                    Text(
                      "80",
                      style: TextStyle(fontSize: 13, color: Colors.black),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "1 Item,1 Package",
                    style: TextStyle(fontSize: 13, color: Colors.black),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    "Total: 22,000",
                    style: TextStyle(fontSize: 13, color: Colors.black),
                  ),
                ],
              ),
              Container(
                alignment: Alignment.centerRight,
                child: Text(
                  "Pain by Cash on Delivery",
                  style: TextStyle(fontSize: 13, color: Colors.black),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
