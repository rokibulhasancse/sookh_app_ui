import 'package:flutter/material.dart';

class MyDropDown extends StatefulWidget {
  const MyDropDown({
    Key? key,
  }) : super(key: key);

  @override
  _MyDropDownState createState() => _MyDropDownState();
}

class _MyDropDownState extends State<MyDropDown> {
  List<String> spinnerItems = [
    "Delivery was Delayed",
    "Wrong Product Delivery",
    "Damaged Product",
    "Payment Related Issues",
    "Other"
  ];
  var selected;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<String>(
      dropdownColor: Colors.grey,
      value: selected,
      items: spinnerItems
          .map((label) => DropdownMenuItem(
                child: Text(label),
                value: label,
              ))
          .toList(),
      onChanged: (value) {
        setState(() => selected = value!);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("" + selected),
        ));
      },
    );
  }
}
