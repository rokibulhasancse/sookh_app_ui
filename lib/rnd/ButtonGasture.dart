// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
//
//
// class ButtonGasture extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo Application', theme: ThemeData(
//       primarySwatch: Colors.green,),
//       home: DemoApp(),
//     );
//   }
// }
//
// class DemoApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return RawGestureDetector(
//       gestures: {
//         AllowMultipleGestureRecognizer: GestureRecognizerFactoryWithHandlers<
//             AllowMultipleGestureRecognizer>(
//               () => AllowMultipleGestureRecognizer(),
//               (AllowMultipleGestureRecognizer instance) {
//             instance.onTap = () => print('It is the parent container gesture');
//           },
//         )
//       },
//       behavior: HitTestBehavior.opaque,
//       //Parent Container
//       child: Container(
//         color: Colors.green,
//         child: Center(
//           //Now, wraps the second container in RawGestureDetector
//           child: RawGestureDetector(
//             gestures: {
//               AllowMultipleGestureRecognizer:
//               GestureRecognizerFactoryWithHandlers<
//                   AllowMultipleGestureRecognizer>(
//                     () => AllowMultipleGestureRecognizer(),  //constructor
//                     (AllowMultipleGestureRecognizer instance) {  //initializer
//                   instance.onTap = () => print('It is the nested container');
//                 },
//               )
//             },
//             //Creates the nested container within the first.
//             child: Container(
//               color: Colors.deepOrange,
//               width: 250.0,
//               height: 350.0,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
//
// class AllowMultipleGestureRecognizer extends TapGestureRecognizer {
//   @override
//   void rejectGesture(int pointer) {
//     acceptGesture(pointer);
//   }
// }
// class MyApp1 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final appTitle = "Flutter Grid List Demo";
//
//     return MaterialApp(
//         title: appTitle,
//         home: Scaffold(appBar: AppBar(
//           title: Text(appTitle),
//         ),
//             body: GridView.count(
//                 crossAxisCount: 3,
//                 children: List.generate(choices.length, (index) {
//                   return Center(
//                     child: SelectCard(choice: choices[index]),
//                   );
//                 }
//                 )
//             )
//         )
//     );
//   }
// }
//
// class Choice {
//   const Choice({this.title, this.icon});
//   final String title;
//   final IconData icon;
// }
//
// const List<Choice> choices = const <Choice>[
//   const Choice(title: 'Home', icon: Icons.home),
//   const Choice(title: 'Contact', icon: Icons.contacts),
//   const Choice(title: 'Map', icon: Icons.map),
//   const Choice(title: 'Phone', icon: Icons.phone),
//   const Choice(title: 'Camera', icon: Icons.camera_alt),
//   const Choice(title: 'Setting', icon: Icons.settings),
//   const Choice(title: 'Album', icon: Icons.photo_album),
//   const Choice(title: 'WiFi', icon: Icons.wifi),
//   const Choice(title: 'GPS', icon: Icons.gps_fixed),
// ];
//
// class SelectCard extends StatelessWidget {
//   const SelectCard({Key key, this.choice}) : super(key: key);
//   final Choice choice;
//
//   @override
//   Widget build(BuildContext context) {
//     final TextStyle textStyle = Theme.of(context).textTheme.display1;
//     return Card(
//         color: Colors.lightGreenAccent,
//         child: Center(child: Column(
//             mainAxisSize: MainAxisSize.min,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: <Widget>[
//               Expanded(child: Icon(choice.icon, size:50.0, color: textStyle.color)),
//               Text(choice.title, style: textStyle),
//             ]
//         ),
//         )
//     );
//   }
// }

// class MyApp extends StatefulWidget {
//   @override
//   _State createState() => _State();
// }
//
// class _State extends State<MyApp> {
//   TextEditingController nameController = TextEditingController();
//   TextEditingController passwordController = TextEditingController();
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: Text('Flutter TextField Example'),
//         ),
//         body: Padding(
//             padding: EdgeInsets.all(15),
//             child: Column(
//               children: <Widget>[
//                 Padding(
//                   padding: EdgeInsets.all(15),
//                   child: TextField(
//                     controller: nameController,
//                     decoration: InputDecoration(
//                       border: OutlineInputBorder(),
//                       labelText: 'User Name',
//                       hintText: 'Enter Your Name',
//                     ),
//                   ),
//                 ),
//                 Padding(
//                   padding: EdgeInsets.all(15),
//                   child: TextField(
//                     controller: passwordController,
//                     obscureText: true,
//                     decoration: InputDecoration(
//                       border: OutlineInputBorder(),
//                       labelText: 'Password',
//                       hintText: 'Enter Password',
//                     ),
//                   ),
//                 ),
//                 RaisedButton(
//                   textColor: Colors.white,
//                   color: Colors.blue,
//                   child: Text('Sign In'),
//                   onPressed: (){
//                showDialog(
//                       context: context,
//                       builder: (context) {
//                         return AlertDialog(
//                           title: Text("Alert Message"),
//                           // Retrieve the text which the user has entered by
//                           // using the TextEditingController.
//                           content: Text(nameController.text),
//                           actions: <Widget>[
//                             new FlatButton(
//                               child: new Text('OK'),
//                               onPressed: () {
//                                 Navigator.of(context).pop();
//                               },
//                             )
//                           ],
//                         );
//                       },
//                     );
//                   },
//                 )
//               ],
//             )
//         )
//     );
//   }
// }
// class Gasture extends StatefulWidget {
//   @override
//   MyHomePageState createState() => new MyHomePageState();
// }
//
// class MyHomePageState extends State<Gasture> {
//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       appBar: new AppBar(
//         title: new Text('Gestures Example'),
//         centerTitle: true,
//       ),
//       body: new Center(child: GestureDetector(
//           onTap: () {
//             print('Box Clicked');
//           },
//           child: Container(
//             height: 60.0,
//             width: 120.0,
//             padding: EdgeInsets.all(10.0),
//             decoration: BoxDecoration(
//               color: Colors.blueGrey,
//               borderRadius: BorderRadius.circular(15.0),
//             ),
//             child: Center(child: Text('Click Me')),
//           )
//       )),
//     );
//   }
// }