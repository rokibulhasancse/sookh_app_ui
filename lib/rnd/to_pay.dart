import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:fluttertestapp/custom/common/custom_container_inside_vertical_row.dart';
import 'package:fluttertestapp/custom/common/custom_order_status_ui.dart';

class HeroAnimation extends StatelessWidget {
  Widget build(BuildContext context) {
    timeDilation = 2.0; // 1.0 means normal animation speed.

    return Scaffold(
      appBar: AppBar(
        title: const Text('Basic Hero Animation'),
      ),
      body: Center(
        child: PhotoHero(
          photo: Image.network("https://static.javatpoint.com/tutorial/flutter/images/flutter-logo.png"),
         // width: 300.0,
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute<void>(
                builder: (BuildContext context) {
                  return Scaffold(
                    appBar: AppBar(
                      title: const Text('To Ship'),
                    ),
                    body: Container(
                      // The blue background emphasizes that it's a new route.
                      color: Colors.lightBlueAccent,
                      padding: const EdgeInsets.all(16.0),
                      alignment: Alignment.topLeft,
                      // child: PhotoHero(
                      //   photo: Image.network("https://static.javatpoint.com/tutorial/flutter/images/flutter-logo.png"),
                      //   width: 100.0,
                      //   onTap: () {
                      //     Navigator.of(context).pop();
                      //   },
                      // ),
                      child: CustomOrderStateUi(),
                    ),
                  );
                }
            ));
          },
        ),
      ),
    );
  }
}
class PhotoHero extends StatelessWidget {

  final photo;
  final VoidCallback? onTap;
  final double? width;
  PhotoHero({ Key? key, this.photo, this.onTap, this.width }) : super(key: key);

  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Hero(
        tag: photo,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child:// Image.asset(
            ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Expanded(
                  child: CusConInsideVerRow(
                      text: "To Pay",
                      width: 100.0,
                      iconData: Icons.car_rental),
                ),
                Expanded(
                  child: CusConInsideVerRow(
                      text: "To Ship",
                      width: 100.0,
                      iconData: Icons.car_rental),
                ),
                Expanded(
                  child: CusConInsideVerRow(
                      text: "To Receive",
                      width: 100.0,
                      iconData: Icons.car_rental),
                ),
                Expanded(
                  child: CusConInsideVerRow(
                      text: "To Review",
                      width: 100.0,
                      iconData: Icons.car_rental),
                ),
              ],
            )
            // fit: BoxFit.contain,
            // ),
          ),
        ),
      ),
    );
  }
}