import 'package:flutter/material.dart';

class ProductViewUi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Product Ui",
      theme: ThemeData(primarySwatch: Colors.green),
      home: MyHomePage(title: 'Complex layout example'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({String? title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.fromLTRB(3.0, 12.0, 3.0, 12.0),
        children: <Widget>[
          ProductBox(
            name: "iPhone",
            description: "iPhone is the top branded phone ever",
            price: 110000,
            image: 'bell.png',
          ),
          ProductBox(
            name: "Android",
            description: "Android is very stylish phone",
            price: 20000,
            image: "bell.png",
          )
        ],
      ),
    );
  }
}

class ProductBox extends StatelessWidget {
  final String? name;
  final String? description;
  final int? price;
  final String? image;

  ProductBox({Key? key, this.name, this.description, this.price, this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2),
      height: MediaQuery.of(context).size.height * .2,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset("assets/" + image!),
          Expanded(
              child: Container(
            padding: EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  this.name!,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(this.description!),
                Text("price:" + this.price.toString())
              ],
            ),
          ))
        ],
      ),
    );
  }
}
