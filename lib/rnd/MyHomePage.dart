import 'package:flutter_svg/svg.dart';
import 'package:fluttertestapp/notification/NofificationRepository.dart';
import 'package:flutter/material.dart';
import 'package:fluttertestapp/model/NotificationModel.dart';

import '../notification/NotificationResponce.dart';

class ListViewHomeLayout extends StatefulWidget {
  @override
  ListViewHome createState() {
    return new ListViewHome();
  }
}

class ListViewHome extends State<ListViewHomeLayout> {
  var mData;
  late NotificationRepository _notificationRepository = new NotificationRepository();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder<NotificationResponce>(
          future: _notificationRepository.getData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              mData = snapshot.data!.data!;
              return Container(
                  padding: EdgeInsets.all(16),
                  child: ListView.builder(
                    //  shrinkWrap: true,
                      itemCount: mData.length,
                      itemBuilder: (context, index) {
                        return Card(
                            child: Container(
                              padding: EdgeInsets.all(12),
                              //  height: MediaQuery.of(context).size.height * .2,
                              child: Column(
                                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  IntrinsicHeight(
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 2,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              //   color: Colors.yellow,
                                            ),
                                            child: Image.network("https://sookh.com/public/"+mData.elementAt(index).banner,   width: 80,
                                                height: 80,
                                              //  fit:BoxFit.fill
                                            ),
                                           // Image(image: "https://sookh.com.public/"+snapshot.data!.data!.elementAt(index).icon+""),

                                            // Icon(icons[1]),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 8,
                                          child: Container(
                                            //height: 50,
                                            // decoration: BoxDecoration(color: Colors.greenAccent),
                                              child: Align(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  mData.elementAt(index).name,
                                                  style: TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.red,
                                                      fontSize: 16),
                                                ),
                                              )),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Card(
                                    margin: const EdgeInsets.fromLTRB(20, 8, 20, 8),
                                    child: IntrinsicHeight(
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Expanded(
                                            flex: 2,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                //   color: Colors.yellow,
                                              ),
                                              child: SvgPicture.network(
                                                "https://sookh.com/public/"+mData.elementAt(index).icon,
                                                height: 40.0,
                                                width: 40.0,
                                             //   color: color,
                                                clipBehavior: Clip.antiAlias,
                                              )

                                              //Image.network("https://sookh.com/public/"+mData.elementAt(index).icon),
                                              //child: Image(image: AssetImage('assets/bell.png')),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 8,
                                            child: Container(
                                                margin: EdgeInsets.all(12),
                                              //height: 50,
                                              // decoration: BoxDecoration(color: Colors.greenAccent),
                                                child: Align(
                                                  alignment: Alignment.centerLeft,
                                                  child: Text("dfjd jf lksjdf fd jsdfgjf jdfg jfghkfd sdfgkfgetiy[woioiwt woeirt ewoirtoeitioet dfjd jf lksjdf fd jsdfgjf jdfg jfghkfd sdfgkfgetiy[woioiwt woeirt ewoirtoeitioet dfjd jf lksjdf fd jsdfgjf jdfg jfghkfd sdfgkfgetiy[woioiwt woeirt ewoirtoeitioet dfjd jf lksjdf fd jsdfgjf jdfg jfghkfd sdfgkfgetiy[woioiwt woeirt ewoirtoeitioet",
                                                      //mData.elementAt(index).name,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 12),
                                                  ),
                                                )),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ));
                      }));
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
    // );
  }
// return Container(
//     padding: EdgeInsets.all(16),
//     child: ListView.builder(
//         //  shrinkWrap: true,
//         itemCount: mData.data!.length,
//         itemBuilder: (context, index) {
//           return Card(
//               child: Container(
//             padding: EdgeInsets.all(12),
//             //  height: MediaQuery.of(context).size.height * .2,
//             child: Column(
//               //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: [
//                 IntrinsicHeight(
//                   child: Row(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Expanded(
//                         flex: 2,
//                         child: Container(
//                           decoration: BoxDecoration(
//                               //   color: Colors.yellow,
//                               ),
//                           child:
//                               Image(image: AssetImage('assets/bell.png')),
//
//                           // Icon(icons[1]),
//                         ),
//                       ),
//                       Expanded(
//                         flex: 8,
//                         child: Container(
//                             //height: 50,
//                             // decoration: BoxDecoration(color: Colors.greenAccent),
//                             child: Align(
//                           alignment: Alignment.centerLeft,
//                           child: Text(
//                             "notiModel.elementAt(index).notiHead",
//                             style: TextStyle(
//                                 fontWeight: FontWeight.bold,
//                                 color: Colors.red,
//                                 fontSize: 18),
//                           ),
//                         )),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Card(
//                   margin: const EdgeInsets.fromLTRB(20, 8, 20, 8),
//                   child: IntrinsicHeight(
//                     child: Row(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: <Widget>[
//                         Expanded(
//                           flex: 2,
//                           child: Container(
//                             decoration: BoxDecoration(
//                                 //   color: Colors.yellow,
//                                 ),
//                             child:
//                                 Image(image: AssetImage('assets/bell.png')),
//
//                             // Icon(icons[1]),
//                           ),
//                         ),
//                         Expanded(
//                           flex: 8,
//                           child: Container(
//                               //height: 50,
//                               // decoration: BoxDecoration(color: Colors.greenAccent),
//                               child: Align(
//                             alignment: Alignment.centerLeft,
//                             child: Text(
//                               "Header Text 1111 Header Text 1111 Header Text 1111 Header Text 1111 Header Text 1111",
//                               style: TextStyle(
//                                   fontWeight: FontWeight.bold,
//                                   color: Colors.red,
//                                   fontSize: 18),
//                             ),
//                           )),
//                         ),
//                       ],
//                     ),
//                   ),
//                   // child: ConstrainedBox(
//                   //   constraints: new BoxConstraints(
//                   //     minHeight: 50.0,
//                   //     minWidth: 50.0,
//                   //     //maxHeight: 300.0,
//                   //     //maxWidth: 500.0,
//                   //   ),
//                   //   child: new DecoratedBox(
//                   //       decoration: new BoxDecoration(color: Colors.green),
//                   //       child: Container(
//                   //         margin: EdgeInsets.fromLTRB(15, 8, 15, 15.0),
//                   //         // padding: EdgeInsets.fromLTRB(15, 15, 15, 15.0),
//                   //         child: Row(
//                   //           //crossAxisAlignment: CrossAxisAlignment.stretch,
//                   //           mainAxisAlignment: MainAxisAlignment.start,
//                   //           crossAxisAlignment: CrossAxisAlignment.start,
//                   //           children: <Widget>[
//                   //             Expanded(
//                   //               flex: 2,
//                   //               child: Container(
//                   //                 // padding: EdgeInsets.fromLTRB(15, 15, 15, 15.0),
//                   //                 //height: MediaQuery.of(context).size.height/4,
//                   //                 decoration:
//                   //                     BoxDecoration(color: Colors.yellow),
//                   //                // child: Icon(icons[1]),
//                   //                 child: Image(image: AssetImage('assets/bell.png')),
//                   //
//                   //               ),
//                   //             ),
//                   //             Expanded(
//                   //               flex: 8,
//                   //               child: Container(
//                   //                 //padding: EdgeInsets.fromLTRB(15, 15, 15, 15.0),
//                   //
//                   //                 decoration: BoxDecoration(
//                   //                     color: Colors.greenAccent),
//                   //                 child: Text(
//                   //                   "Header Text 0000 Header Text 0000 Header Text 0000 Header Text 0000",
//                   //                   //style: TextStyle(fontWeight: FontWeight.bold),
//                   //                 ),
//                   //               ),
//                   //             ),
//                   //           ],
//                   //         ),
//                   //       )),
//                   // )
//                 ),
//               ],
//             ),
//           ));
//         }));
}
//}
