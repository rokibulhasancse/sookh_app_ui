
import 'package:fluttertestapp/custom/common/CustomAppBar.dart';
import 'package:fluttertestapp/custom/common/custom_notification_ui.dart';
import 'package:fluttertestapp/model/NotificationModel.dart';
import 'package:flutter/material.dart';

class AppNotificationPage extends StatefulWidget {
  @override
  ListViewHome createState() {
    return new ListViewHome();
  }
}

class ListViewHome extends State<AppNotificationPage> {
  List<NotificationModel> notiModel = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    notiModel.add(new NotificationModel(1, "Computer", "", "",
        "A computer is a machine that can store and process information. Most computers rely on a binary system that uses two variables, 0 and 1, to complete tasks such as storing data, calculating algorithms, and displaying information."));
    notiModel.add(new NotificationModel(1, "Laptop", "", "",
        "Laptops combine all the input/output components and capabilities of a desktop computer, including the display screen, small speakers, a keyboard, data storage device, sometimes an optical disc drive, pointing devices (such as a touchpad or pointing stick), with an operating system, a processor and memory into a single"));
    notiModel.add(new NotificationModel(1, "Android", "", "",
        "Android is a mobile operating system based on a modified version of the Linux kernel and other open source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. ... Some well known derivatives include Android TV for televisions and Wear OS for wearables, both developed by Google."));
    notiModel.add(
        new NotificationModel(1, "IPhone", "", "", "Details of the product"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false, // Used for removing back buttoon.
          backgroundColor: Colors.grey.shade50,
          title: CustomAppBar(
            title: " Notification",
          ),
        ),
        body: CustomNotificationUi(notiModel));
  }
}
