class Assets {
	static const String dog = "assets/dog.png";
	static const String bell = "assets/bell.png";
	static const String instragram = "assets/instragram.jpeg";
	static const String sophia = "assets/images/sophia.jpg";
	static const String john = "assets/images/john.jpg";
	static const String olivia = "assets/images/olivia.jpg";
	static const String greg = "assets/images/greg.jpg";
	static const String steven = "assets/images/steven.jpg";
	static const String james = "assets/images/james.jpg";
	static const String sam = "assets/images/sam.jpg";
	static const String app_logo = "assets/app_logo.png";
}

