import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertestapp/chat/MessagePage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertestapp/custom/common/CustomAppBar.dart';

class ReviewPage extends StatefulWidget {
  @override
  ProductReview createState() {
    return new ProductReview();
  }
}

class ProductReview extends State<ReviewPage> {
  bool _checkbox = false;
  var radioItem;
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false, // Used for removing back buttoon.
        backgroundColor: Colors.grey.shade50,
        title: CustomAppBar(
          title: " Review",
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(top: 40, bottom: 16, left: 16, right: 16),
        child: ListView(children: [
          IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    decoration: BoxDecoration(),
                    child: Image(
                        image: AssetImage(
                            'assets/bell.png')), //Icon(Icons.account_tree)//
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Container(
                      margin: EdgeInsets.only(top: 8, bottom: 8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Color Family: BLACK,Size:Int:One size",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 12),
                          ),
                          Text(
                            "Lotto 800% Bonus Privilege Card",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      )
                      ),
                ),
              ],
            ),
          ),
          Divider(
            height: 1,
            color: Colors.grey,
          ),
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: 16, bottom: 16),
            child: RatingBar.builder(
              initialRating: 3,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ),
          InkWell(
            child: Container(
              padding: EdgeInsets.all(8),
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.black)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.camera),
                  Text(
                    "Upload Photo",
                    style: TextStyle(
                      fontSize: 13,
                    ),
                  )
                ],
              ),
              // ),
            ),
            onTap: () {},
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            child: TextFormField(
              minLines: 2,
              maxLines: 5,
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                hintText: "Don't be shy, tell us more!",
                hintStyle: TextStyle(color: Colors.grey, fontSize: 12),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(1.0)),
                ),
              ),
            ),
          ),
          Row(
            children: [
              Radio(
                  value: 2,
                  toggleable: true,
                  groupValue: radioItem,
                  onChanged: (val) {
                    if (isSelected) {
                      setState(() {
                        isSelected = true;
                        radioItem = val;
                      });
                    } else {
                      setState(() {
                        isSelected = false;
                        radioItem = val;
                      });
                    }
                  }),
              Text(
                "Anonymous",
                style: TextStyle(fontSize: 12),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.all(8),
            color: Colors.grey,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(Icons.car_rental),
                Text(
                  "Rate your Rider",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: 8),
            child: RatingBar.builder(
              initialRating: 3,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            child: TextFormField(
              minLines: 2,
              maxLines: 5,
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                hintText: "How was your overall experience with our rider?",
                hintStyle: TextStyle(color: Colors.grey, fontSize: 12),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(1.0)),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            padding: EdgeInsets.all(8),
            color: Colors.grey,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(Icons.car_rental),
                Text(
                  "Seller Service",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16, bottom: 4),
            alignment: Alignment.center,
            child: RatingBar.builder(
              initialRating: 3,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ),
          Row(
            children: [
              Checkbox(
                value: _checkbox,
                onChanged: (value) {
                  setState(() {
                    _checkbox = !_checkbox;
                  });
                },
              ),
              Text(
                "I agree to Sookh's Privacy Policy",
                style: TextStyle(fontSize: 12),
              ),
            ],
          ),
          InkWell(
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.deepOrangeAccent,
                  borderRadius: BorderRadius.circular(5)),
              padding: EdgeInsets.all(12),
              child: Text(
                "Share Review",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 13),
              ),
            ),
            onTap: () {
              Navigator.pop(context);
            },
          )
        ]),
      ),
      //  )
    );
  }
}
