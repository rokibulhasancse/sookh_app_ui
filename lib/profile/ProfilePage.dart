import 'package:flutter/material.dart';
import 'package:fluttertestapp/custom/common/CustomRowMyService.dart';
import 'package:fluttertestapp/custom/common/custom_profile_app_bar.dart';
import 'package:fluttertestapp/custom/common/hotizontal_slider_with_indegator.dart';
import 'package:fluttertestapp/custom/common/order_grid_view.dart';

class ProfilePage extends StatefulWidget {
  @override
  ListViewHome createState() {
    return new ListViewHome();
  }
}

class ListViewHome extends State<ProfilePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          child: CustomProfileAppBar(),
          preferredSize: Size.fromHeight(180),
        ),
        body: Container(
            color: Colors.white12,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        " My Orders",
                        style: TextStyle(
                            fontSize: 13, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "View All >",
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.deepOrange,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Expanded(child:
                OrderGridView()

                ),
                Expanded(
                    child: HorizontalSliderWithIndegator(),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                  child: Text(
                    "    My Service",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
                  ),
                ),
                Expanded(child: CustomRowMyService()),
              ],
            )));
  }
}
