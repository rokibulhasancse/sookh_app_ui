import 'package:flutter/material.dart';
import 'chat/MessagePage.dart';
import 'chat/chatHome.dart';
import 'custom/common/CarouselDemo.dart';
import 'custom/common/hotizontal_slider_with_indegator.dart';
import 'profile/ProfilePage.dart';
import 'voucher/Voucher.dart';
import 'voucher/VoucherDetails.dart';

void main() {
  runApp(new MaterialApp(home: new MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home:
        ProfilePage()//
            //Profile()
        // MessagePage(),// todo review page ui
        //Voucher()
        );
  }
}

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red.shade200,
      body:
      Container(
        decoration: BoxDecoration(border: Border.all(color: Colors.black),color: Colors.orange.shade200),

        //color: Colors.orange.shade200,
        margin: EdgeInsets.only(top: 78,bottom: 40,left: 40,right: 40),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Sookh",
              style: TextStyle(
                  fontSize: 32,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            Text("Happy Shopping",
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),),
            Padding(
              padding: EdgeInsets.only(top: 16,bottom: 16),
              child: CircleAvatar(
                radius: 65,
                backgroundImage: AssetImage("assets/images/steven.jpg"),
              ),
            ),
            Text("Rokibul Hasan",   style: TextStyle(
                fontSize: 20,
                color: Colors.black,
                fontWeight: FontWeight.bold),),
            Text("Apps Developer" , style: TextStyle(
                fontSize: 16,
                color: Colors.black,
                )),
      Container(
        padding: EdgeInsets.all(16),

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(text: TextSpan(
                children: [
                  TextSpan(text: "ID No : ", style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                  ),

                  TextSpan(text: "Im51515151:", style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                  ),
                ]
            )),
            RichText(text: TextSpan(
                children: [
                  TextSpan(text: "Blood Group: ", style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                  ),

                  TextSpan(text: "B+", style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                  ),
                ]
            ),),
            Padding(
              padding: EdgeInsets.only(top: 16),
              child: RichText(text: TextSpan(
                  children: [
                    TextSpan(text: "Join Date : ", style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.bold
                    ),
                    ),

                    TextSpan(text: "12-1212-12", style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                        fontWeight: FontWeight.bold
                    ),
                    ),
                  ]
              )),
            ),
            RichText(text: TextSpan(
                children: [
                  TextSpan(text: "Expire Date: ", style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                  ),

                  TextSpan(text: "12-131-12", style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                  ),
                ]
            ),)

          ],
        ),
      ),
  
          ],
        ),
      ),

    );
  }
}
